import React from 'react';
import './ToggleButton.css';

const ToggleButton = props => {

    let buttonClasses = ['ToggleButton'];

    if(props.type === 1)
    {
        buttonClasses.push('More');
    }
    if(props.ingrCount === 0)
    {
        var disable = true;
    }
    else if(props.type === 0)
    {
        buttonClasses.push('Less');
    }

    return (
        <div className={props.text}>
            <button className={buttonClasses.join(' ')} 
                onClick={props.click} disabled={disable}> 
                {props.text} 
            </button>
        </div>
    )

}

export default ToggleButton;