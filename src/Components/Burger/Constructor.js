import React from 'react';
import './Burger.css';

const Constructor = (props) => {
let bacons =[];
let meats =[];
let salads =[];
let cheeses =[];
for(var i = 0; i<props.baconCount;i++)
{
  bacons.push(<div className="Bacon"></div>)
}
for(var i = 0; i<props.cheeseCount;i++)
{
  cheeses.push(<div className="Cheese"></div>)
}
for(var i = 0; i<props.meatCount;i++)
{
  meats.push(<div className="Meat"></div>)
}
for(var i = 0; i<props.saladCount;i++)
{
  salads.push(<div className="Salad"></div>)
}
  return (
    <div className="Burger">
      <div className="BreadTop">
        <div className="Seeds1"></div>
        <div className="Seeds2"></div>
      </div>
      {salads}
      {bacons}
      {cheeses}
      {meats}
      <div className="BreadBottom"></div>
    </div>
  )
}



export default Constructor;