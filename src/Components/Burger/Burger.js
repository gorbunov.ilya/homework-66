import React from 'react';
import Ingredient from './Ingredient';
const Burger = props => {

    return <div className="products"><p>Current price: <b>{props.totalPrice}</b></p>
       {props.ingredients.map((ingr) => {
        return <Ingredient
          key={ingr.id}
          count={ingr.count}
          name={ingr.name}
          price={ingr.price}
          totaPrice={props.totaPrice}
          increase={() => props.increase(ingr.id)}
          decrease={() => props.decrease(ingr.id)}>
        </Ingredient>
      })}
    </div>
    
}

export default Burger;