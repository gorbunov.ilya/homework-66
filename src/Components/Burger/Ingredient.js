import React from 'react';
import ToggleButton from '../ToggleButton/ToggleButton';

const Ingredient = props => {
    return (
        <div className="buttons">
            <b>{props.name}</b>
            <ToggleButton text='Less' type={0} click={props.decrease} ingrCount={props.count} />
            <ToggleButton text='More' type={1} click={props.increase}/>
        </div>
    )
}

export default Ingredient;