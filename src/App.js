import React, { Component } from 'react';
import './App.css';
import Burger from './Components/Burger/Burger';
import Constructor from './Components/Burger/Constructor';
import './Components/Burger/Products.css'
class App extends Component {

  state = {
    currentPrice: 20,
    
    ingredients: [
      { name: 'Salad', count: 0, id: 1, price: 5 },
      { name: 'Bacon', count: 0, id: 2, price: 30 },
      { name: 'Cheese', count: 0, id: 3, price: 20 },
      { name: 'Meat', count: 0, id: 4, price: 50 }
    ]
  }
  productName = '';
  productCount = 0;

  increase = (id) => {
    const index = this.state.ingredients.findIndex(p => p.id === id);
    const ingredients = [...this.state.ingredients];
    let newPrice = this.state.currentPrice;
    newPrice += ingredients[index].price;
    ingredients[index].count++;
    this.productCount=ingredients[index].count
    this.productName=ingredients[index].name
    this.setState({ currentPrice: newPrice }, function () {
      console.log(this.state.currentPrice)
    });
  }

  decrease = (id) => {
    const index = this.state.ingredients.findIndex(p => p.id === id);
    const ingredients = [...this.state.ingredients];
    if(ingredients[index].count !== 0)
    {
      let newPrice = this.state.currentPrice;
    newPrice -= ingredients[index].price;
    ingredients[index].count--;
    this.productCount=ingredients[index].count;
    this.productName=ingredients[index].name;
    this.setState({ currentPrice: newPrice }, function () {
      console.log(this.state.currentPrice)
    });
    }
  }
  getProductCount = (id) =>{
    const index = this.state.ingredients.findIndex(p => p.id === id);
    const ingredients = [...this.state.ingredients];
    return ingredients[index].count;
  }

  render() {
    return (
      <div className="App">
        <Constructor 
        baconCount={this.getProductCount(2)}
        meatCount={this.getProductCount(4)}
        cheeseCount={this.getProductCount(3)}
        saladCount={this.getProductCount(1)}
        />
          <Burger ingredients={this.state.ingredients}
            increase={this.increase}
            decrease={this.decrease}
            totalPrice={this.state.currentPrice} />
      </div>
    );

  }


}

export default App;
